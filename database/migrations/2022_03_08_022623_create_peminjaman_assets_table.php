<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeminjamanAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman_assets', function (Blueprint $table) {
            $table->id();
            $table->string('nama_peminjam');
            $table->string('nomor_telepon');
            $table->string('tanggal_peminjaman');
            $table->string('sampai_tanggal');
            $table->string('assets_id');
            $table->string('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjaman_assets');
    }
}
