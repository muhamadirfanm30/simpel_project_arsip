<?php

use App\Http\Controllers\InputAssetsController;
use App\Http\Controllers\PeminjamanAssetsController;
use App\Models\PeminjamanAsset;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout-users', [App\Http\Controllers\HomeController::class, 'logoutUser'])->name('logout-users');

Route::middleware('auth')->group(function () {
    Route::prefix('admin')->group(function () {
        //product

        Route::prefix('input-assets')->group(function () {
            Route::get('/', [InputAssetsController::class, 'index'])->name('input.assets.index');
            Route::get('/create', [InputAssetsController::class, 'create'])->name('input.assets.create');
            Route::get('/update/{id}', [InputAssetsController::class, 'update'])->name('input.assets.update');
            Route::post('/store', [InputAssetsController::class, 'store'])->name('input.assets.store');
            Route::post('/edit/{id}', [InputAssetsController::class, 'edit'])->name('input.assets.edit');
            Route::post('/destroy/{id}', [InputAssetsController::class, 'destroy'])->name('input.assets.destroy');
        });

        Route::prefix('pinjam-assets')->group(function () {
            Route::get('/', [PeminjamanAssetsController::class, 'index'])->name('peminjaman.aset.index');
            Route::get('/create', [PeminjamanAssetsController::class, 'create'])->name('peminjaman.aset.create');
            Route::get('/update/{id}', [PeminjamanAssetsController::class, 'update'])->name('peminjaman.aset.update');
            Route::get('/barcode-peminjaman/{id}', [PeminjamanAssetsController::class, 'generateBarcode'])->name('peminjaman.aset.barcode');
            Route::post('/store', [PeminjamanAssetsController::class, 'store'])->name('peminjaman.aset.store');
            Route::post('/edit/{id}', [PeminjamanAssetsController::class, 'edit'])->name('peminjaman.aset.edit');
            Route::post('/destroy/{id}', [PeminjamanAssetsController::class, 'destroy'])->name('peminjaman.aset.destroy');
        });
        
    });
});
