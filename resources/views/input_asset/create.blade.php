@extends('home')
@section('content')
    <form action="{{ route('input.assets.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div data-role="dynamic-fields">
            <div class="form-inline">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-11">
                                    <h3 class="card-title">
                                        Tambah Aset Baru
                                    </h3>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-danger" data-role="remove">
                                        <span class="fa fa-trash"></span>
                                    </button>
                                    <button class="btn btn-primary" data-role="add">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Nama Aset </label>
                                            </div>
                                            <div class="col-md-11">
                                                <input type="text" name="nama[]" class="form-control" value="{{Request::old('nama[]')}}" placeholder="Nama Aset ..." style="width:100%">
                                            </div>
                                        </div>
                                    </div><br><br><br>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Stok </label>
                                            </div>
                                            <div class="col-md-11">
                                                <input type="number" name="stok[]" class="form-control" value="{{Request::old('stok[]')}}" placeholder="Stok ..." style="width:100%">
                                            </div>
                                        </div>
                                    </div><br><br><br>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Keterangan </label>
                                            </div>
                                            <div class="col-md-11">
                                                <input type="text" name="desc[]" class="form-control" value="{{Request::old('desc[]')}}" placeholder="Keterangan ..." style="width:100%">
                                            </div>
                                        </div>
                                    </div><br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!-- /div.form-inline -->
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
        </div><br><br><br>
    </form>

<style>

    [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
        display: none;
    }

    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
        display: inline-block;
        float: right;
    }

    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
        display: none;
        float: right;
    }
</style>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script>
    $(function() {
        // Remove button click
        $(document).on(
            'click',
            '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
            function(e) {
                e.preventDefault();
                $(this).closest('.form-inline').remove();
            }
        );
        // Add button click
        $(document).on(
            'click',
            '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
            function(e) {
                e.preventDefault();
                var container = $(this).closest('[data-role="dynamic-fields"]');
                new_field_group = container.children().filter('.form-inline:first-child').clone();
                new_field_group.find('input').each(function(){
                    $(this).val('');
                });
                container.append(new_field_group);
            }
        );
    });
</script>
@endsection