@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <!-- <a href="{{ url('/admin/input-assets/edit/'.$data_edit->id) }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH ASSET BARU</a><br> -->
        </div>
    </div>
    <form action="{{ url('/admin/input-assets/edit/'.$data_edit->id) }}" method="post">
        @csrf 
        <div class="card-body">
            <div class="form-group">
                <label for="">Nama</label>
                <input type="text" name="nama" class="form-control" value="{{ $data_edit->nama }}">
            </div>
            <div class="form-group">
                <label for="">Stok</label>
                <input type="text" name="stok" class="form-control" value="{{ $data_edit->stok }}">
            </div>

            <div class="form-group">
                <label for="">Keterangan</label>
                <input type="text" name="desc" class="form-control" value="{{ $data_edit->desc }}">
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6"><a href="{{ url('admin/input-assets') }}" class="btn btn-info btn-block">Kembali</a></div>
                        <div class="col-md-6"><button type="submit" class="btn btn-success btn-block">Simpan</button></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection