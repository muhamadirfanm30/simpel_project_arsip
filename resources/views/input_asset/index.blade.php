@extends('home')
@section('content')

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/input-assets/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH ASSET BARU</a><br>
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>Nama Barang</th>
                <th>Stok</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($list_assets as $list)
                    <tr>
                        <td>{{ $list->nama }}</td>
                        <td>{{ $list->stok }}</td>
                        <td>
                            <form action="{{ url('admin/input-assets/destroy/'.$list->id) }}" method="post">
                                @csrf
                                <a href="{{ url('admin/input-assets/update/'.$list->id) }}" class="btn btn-success btn-sm">Update</a>
                                <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('Apakah Anda Yakin Akan menghapus Data Ini ?')"> Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection