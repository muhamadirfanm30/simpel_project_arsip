@extends('home')
@section('content')

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <!-- <a href="{{ url('/admin/input-assets/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH DATA PEMINJAM</a><br> -->
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/pinjam-assets/edit/'.$edit_aset->id) }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Nama Peminjam</label>
                        <input type="text" name="nama_peminjam" class="form-control" value="{{ $edit_aset->nama_peminjam }}" placeholder="Nama Peminjam .... " >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Nomor Telepon Peminjam</label>
                        <input type="number" name="nomor_telepon" class="form-control" value="{{ $edit_aset->nomor_telepon }}" placeholder="Nomor telepon Peminjam .... " >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Dari Tanggal</label>
                        <input type="date" name="tanggal_peminjaman" class="form-control" value="{{ $edit_aset->tanggal_peminjaman }}" placeholder="Nomor telepon Peminjam .... " >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Sampai Tanggal</label>
                        <input type="date" name="sampai_tanggal" class="form-control" value="{{ $edit_aset->sampai_tanggal }}" placeholder="Nomor telepon Peminjam .... " >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Barang yang Dipinjam</label>
                        <select name="assets_id" class="form-control" id="" >
                            <option value="">Pilih ....</option>
                            @foreach($list_asstes as $val)
                                <option value="{{ $val->id }}" {{ $val->id == $edit_aset->assets_id ? 'selected' : '' }}>{{ $val->nama }} - Stok : ({{ $val->stok }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Jumlah</label>
                        <input type="number" name="qty" class="form-control" value="{{ $edit_aset->qty }}" placeholder="Qty .... " >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" class="form-control" id="">
                            <option value="">Pilih ....</option>
                            <option value="1" {{ $edit_aset->status == 1 ? 'selected' : '' }}>Dipinjam</option>
                            <option value="2" {{ $edit_aset->status == 2 ? 'selected' : '' }}>Dikembalikan</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6">
                    <a href="{{ url('admin/pinjam-assets') }}" class="btn btn-info btn-block">Batal</a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success btn-block">Edit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection