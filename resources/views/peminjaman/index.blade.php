@extends('home')
@section('content')

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/pinjam-assets/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH DATA PEMINJAM</a><br>
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>Nama Peminjam</th>
                <th>Barang Dipinjam</th>
                <th>Stok</th>
                <th>Dari Tanggal</th>
                <th>Sampai Tangal</th>
                <!-- <th>Total Hari</th> -->
                <th>Status Peminjaman</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($list_peminjaman as $values)
                <!-- status 1 dipinjam 2 dikembalikan -->
                    <tr>
                        <td>{{ $values->nama_peminjam }}</td>
                        <td>{{ !empty($values->assets) ? $values->assets->nama : 'Not Found' }}</td>
                        <td>{{ $values->qty }}</td>
                        <td>{{ $values->tanggal_peminjaman }}</td>
                        <td>{{ $values->sampai_tanggal }}</td>
                        <!-- <td>{{ $values->jumlah_hari }} Hari</td> -->
                        <td>{{ $values->status_peminjaman }}</td>
                        <td>
                            <form action="" method="post">
                                @csrf
                                <a href="{{ url('admin/pinjam-assets/update/'.$values->id) }}" class="btn btn-primary btn-sm">Update</a>
                                <a href="https://web.whatsapp.com/send?phone={{ $values->nomor_telepon }}" target="_blank" class="btn btn-success btn-sm">Whatsapp</a>
                                <a href="{{ url('admin/pinjam-assets/barcode-peminjaman/'.$values->id) }}" target="_blank" class="btn btn-info btn-sm">barcode</a>
                                <!-- <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('Apakah Anda Yakin Akan menghapus Data Ini ?')"> Delete</button> -->
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection