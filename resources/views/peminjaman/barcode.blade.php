@extends('home')
@section('content')

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/pinjam-assets') }}" class="btn btn-primary add-banners"> Kembali</a><br>
        </div>
    </div>
    <div class="card-body">
        <center>
        {!! $qrcode !!}
        </center>
    </div>
</div>
@endsection