<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputAsset extends Model
{
    use HasFactory;
    protected $table = 'input_assets';
    protected $fillable = [
        'nama',
        'stok',
        'desc',
    ];
}
