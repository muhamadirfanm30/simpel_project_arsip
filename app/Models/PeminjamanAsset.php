<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeminjamanAsset extends Model
{
    use HasFactory;
    protected $table = 'peminjaman_assets';
    protected $fillable = [
        'nama_peminjam',
        'nomor_telepon',
        'tanggal_peminjaman',
        'sampai_tanggal',
        'assets_id',
        'qty',
        'status'
    ];

    protected $appends = [
        'jumlah_hari',
        'status_peminjaman',
    ];

    public function getJumlahHariAttribute()
    {
        $dStart = new \DateTime($this->tanggal_peminjaman);
        $dEnd  = new \DateTime($this->sampai_tanggal);
        $dDiff = $dStart->diff($dEnd);
        return $dDiff->format('%r%a');
    }

    public function getStatusPeminjamanAttribute()
    {
        $tgl_sekarang = new \DateTime();
        $tgl_ahir  = new \DateTime($this->sampai_tanggal);
        if($tgl_sekarang > $tgl_ahir){
            return 'Waktu Sudah Habis';
        }else{
            $dDiff = $tgl_sekarang->diff($tgl_ahir);
            return 'Tersisa ' . $dDiff->format('%r%a') . ' Hari masa Peminjaman';
        }
    }

    /**
     * Get the user that owns the PeminjamanAsset
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assets()
    {
        return $this->belongsTo(InputAsset::class, 'assets_id', 'id');
    }
}
