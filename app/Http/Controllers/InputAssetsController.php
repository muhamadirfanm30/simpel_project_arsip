<?php

namespace App\Http\Controllers;

use App\Models\InputAsset;
use Illuminate\Http\Request;

class InputAssetsController extends Controller
{
    public function index()
    {
        return view('input_asset.index', [
            'title' => 'Daftar Aset',
            'list_assets' => InputAsset::get()
        ]);
    }

    public function update($id)
    {
        return view('input_asset.update', [
            'title' => 'Daftar Aset',
            'data_edit' => InputAsset::find($id)
        ]);
    }

    public function create()
    {
        return view('input_asset.create', [
            'title' => 'Tambah Aset Baru',
        ]);
    }

    public function edit(Request $request, $id)
    {
        $editForm = InputAsset::find($id);
        $editForm->update([
            'nama' => $request->nama,
            'stok' => $request->stok,
            'desc' => $request->desc,
        ]);
        return redirect()
            ->route('input.assets.index')
            ->with('success', 'oke');
    }

    public function destroy($id)
    {
        $deleteData = InputAsset::find($id);
        $deleteData->delete();
        return redirect()
            ->route('input.assets.index')
            ->with('success', 'oke');
    }

    public function store(Request $request)
    {
        $name = count($request->nama);
        for ($i = 0; $i < $name; $i++) {
            $insert[] = [
                'nama' => $request->nama[$i],
                'stok' => $request->stok[$i],
                'desc' => $request->desc[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        InputAsset::insert($insert);

        return redirect()
            ->route('input.assets.index')
            ->with('success', 'oke');
    }
}
