<?php

namespace App\Http\Controllers;

use App\Models\InputAsset;
use App\Models\PeminjamanAsset;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PeminjamanAssetsController extends Controller
{
    public function index()
    {
        // return PeminjamanAsset::with('assets')->get();
        return view('peminjaman.index', [
            'title' => 'Daftar Aset Dipinjam',
            'list_peminjaman' => PeminjamanAsset::with('assets')->get()
        ]);
    }

    public function create()
    {
        return view('peminjaman.create', [
            'title' => 'Peminjaman Aset',
            'list_asstes' => InputAsset::where('stok', '>', '0')->get()
        ]);
    }

    public function update($id)
    {
        return view('peminjaman.update', [
            'title' => 'Edit Data Peminjaman Aset',
            'list_asstes' => InputAsset::where('stok', '>', '0')->get(),
            'edit_aset' => PeminjamanAsset::find($id)
        ]);
    }

    public function generateBarcode($id)
    {
        $data = PeminjamanAsset::with('assets')->findOrFail($id);
        $qrcode = QrCode::size(300)->generate($data->nama_peminjam);
        return View('peminjaman.barcode', [
            'title' => 'Barcode',
            'qrcode' => $qrcode,
            'data' => $data,
        ]);
    }

    public function store(Request $request)
    {
        $cekAvailableAssets = InputAsset::where('id', $request->assets_id)->first();
        if($request->qty <= $cekAvailableAssets->stok){

            $saveDataPeminjam = PeminjamanAsset::create([
                'nama_peminjam' => $request->nama_peminjam ,
                'nomor_telepon' => $request->nomor_telepon ,
                'tanggal_peminjaman' => $request->tanggal_peminjaman ,
                'sampai_tanggal' => $request->sampai_tanggal ,
                'assets_id' => $request->assets_id ,
                'qty' => $request->qty ,
                'status' => 1 ,
            ]);
            
            $cekAvailableAssets->update([
                'stok' => $cekAvailableAssets->stok - $request->qty
            ]);

            return redirect()->route('peminjaman.aset.index')->with('success', 'oke');
        }else{
            return redirect()->back()->with('error', 'stok yg anda gunakan melebihi batas');
        }
        
    }

    public function edit(Request $request, $id)
    {
        $cekAvailableAssets = InputAsset::where('id', $request->assets_id)->first();
        $saveDataPeminjam = PeminjamanAsset::find($id);
        
        $total_sebelumnya = $saveDataPeminjam->qty;
        $total_baru = $request->qty;
        $selisih =  $total_baru - $total_sebelumnya;
        
        if($request->qty <= $cekAvailableAssets->stok){

            $saveDataPeminjam->update([
                'nama_peminjam' => $request->nama_peminjam ,
                'nomor_telepon' => $request->nomor_telepon ,
                'tanggal_peminjaman' => $request->tanggal_peminjaman ,
                'sampai_tanggal' => $request->sampai_tanggal ,
                'assets_id' => $request->assets_id,
                'qty' => $request->qty,
                'status' => $request->status,
            ]);

            if($selisih > 0){
                $cekAvailableAssets->update([
                    'stok' => $cekAvailableAssets->stok - $selisih
                ]);
            }else{
                $cekAvailableAssets->update([
                    'stok' => $cekAvailableAssets->stok + abs($selisih)
                ]);
            }

            return redirect()->route('peminjaman.aset.index')->with('success', 'oke');
        }else{
            return redirect()->back()->with('error', 'stok yg anda gunakan melebihi batas');
        }
    }

    public function destroy($id)
    {
        $findData = PeminjamanAsset::find($id);
        $findData->delete();
        return redirect()->route('peminjaman.aset.index')->with('success', 'oke');
    }
}
